
resource "aws_iam_role" "ec2_codeartifact_ecr_access_role" {
  name               = "ec2-codeartifact-ecr-access-role"
  assume_role_policy = "${file("${path.module}/ec2_assume_role_policy.json")}"
}

# data "template_file" "ec2_codeartifact_ecr_access_policy_template" {
#   template = "${file("${path.module}/ec2_codeartifact_ecr_policy.json")}"
# }

resource "aws_iam_policy" "ec2_codeartifact_ecr_access_policy" {
  name = "ec2-codeartifact-ecr-access-policy"
  policy = "${file("${path.module}/ec2_codeartifact_ecr_policy.json")}"
}

resource "aws_iam_policy_attachment" "ec2_codeartifact_ecr_access_policy_attach" {
  name       = "ec2-codeartifact-ecr-access-policy-attach"
  roles      = ["${aws_iam_role.ec2_codeartifact_ecr_access_role.name}"]
  policy_arn = "${aws_iam_policy.ec2_codeartifact_ecr_access_policy.arn}"
}

resource "aws_iam_instance_profile" "ec2_codeartifact_ecr_access_profile" {
  name  = "ec2-codeartifact-ecr-access-profile"
  role = "${aws_iam_role.ec2_codeartifact_ecr_access_role.name}"
}