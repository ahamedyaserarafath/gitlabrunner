# Below resource is to create public key

resource "tls_private_key" "sskeygen_execution" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Below are the aws key pair
resource "aws_key_pair" "gitlab_key_pair" {
  depends_on = ["tls_private_key.sskeygen_execution"]
  key_name   = "${var.aws_public_key_name}"
  public_key = "${tls_private_key.sskeygen_execution.public_key_openssh}"
}

resource "aws_instance" "gitlab_instance" {
  depends_on = [
    aws_route.gitlab_server_internet_access,
    aws_security_group_rule.gitlab_server_security_group_rule_egress_open,
    aws_security_group_rule.gitlab_server_security_group_rule_ingress_tcp_open
  ]   
  count         = "${length(var.aws_availability_zone)}"
  ami           = "${lookup(var.aws_amis,var.aws_region)}"
  instance_type = "${var.aws_instance_type}"
  key_name      = "${aws_key_pair.gitlab_key_pair.id}"
  vpc_security_group_ids = ["${aws_security_group.gitlab_server_security_group_open.id}"]
  subnet_id     = "${aws_subnet.gitlab_server_subnet[count.index].id}"
  iam_instance_profile = "${aws_iam_instance_profile.ec2_codeartifact_ecr_access_profile.name}"

  connection {
    user        = "ubuntu"
    host = self.public_ip
    private_key = "${tls_private_key.sskeygen_execution.private_key_pem}"
  }
  provisioner "remote-exec" { 
    inline = [
      "sudo apt update",
      "sudo apt -y install apt-transport-https ca-certificates curl software-properties-common unzip",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
      "sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable'",
      "sudo apt update",
      "sudo apt -y install docker-ce unzip",
      "public_ip=$(curl -s 'http://169.254.169.254/latest/meta-data/public-ipv4')",
      "curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb",
      "sudo dpkg -i gitlab-runner_amd64.deb",
<<EOT
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "J_s1ofq84xvWvJWsi5vw" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner" \
  --tag-list "docker,aws,chabee" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
sudo sed -i "s@concurrent = .*@concurrent = 5@g" /etc/gitlab-runner/config.toml
sudo sed -i "s@privileged = .*@privileged = true@g" /etc/gitlab-runner/config.toml
sudo sed -i "s@volumes = .*@volumes = [\"/var/run/docker.sock:/var/run/docker.sock\", \"/cache\",\"/config/:/cfg/\"]@g" /etc/gitlab-runner/config.toml
sudo gitlab-runner restart
# Install awscli latest version
sudo curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
sudo unzip awscliv2.zip
sudo ./aws/install -i /usr/local/aws-cli -b /usr/local/bin
# sudo aws configure set aws_access_key_id ${var.aws_key}
# sudo aws configure set aws_secret_access_key ${var.aws_secret}
# sudo mkdir -p /config/
# for ecr config
# sudo aws ecr get-login-password --region ap-southeast-1 > /tmp/ecr_config
# sudo mv /tmp/ecr_config /config/ecr_config
# for code artificats
# sudo rm -rf ~/.aws
EOT

    ]
  }
  provisioner "local-exec" {
    command = "rm -rf ${aws_key_pair.gitlab_key_pair.id}.pem; echo '${tls_private_key.sskeygen_execution.private_key_pem}' > ${aws_key_pair.gitlab_key_pair.id}.pem ; chmod 400 ${aws_key_pair.gitlab_key_pair.id}.pem"
  }
  tags = {
    Name  = "gitlab-server-${count.index + 1}"
    Environment = "${var.env}"
  }
  provisioner "remote-exec" {
      when    = destroy
      inline = [
        "sudo gitlab-runner unregister --all-runners",
      ]
  }  
}


# resource null_resource gitlab_install {
#   depends_on = [
#     aws_instance.gitlab_instance
#   ]
#   connection {
#     user        = "ubuntu"
#     host        = aws_instance.gitlab_instance[0].public_ip
#     private_key = "${tls_private_key.sskeygen_execution.private_key_pem}"
#   }
#   provisioner "remote-exec" {
#     inline = [
#       "curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb",
#       "sudo dpkg -i gitlab-runner_amd64.deb",
# <<EOT
# sudo gitlab-runner register \
#   --non-interactive \
#   --url "https://gitlab.com/" \
#   --registration-token "wewSCLSymeUBvaUEhjEx" \
#   --executor "shell" \
#   --docker-image alpine:latest \
#   --description "docker-runner" \
#   --tag-list "docker,aws" \
#   --run-untagged="true" \
#   --locked="false" \
#   --access-level="not_protected"
# EOT

#     ]
#   }
#     provisioner "remote-exec" {
#       when    = destroy
#       inline = [
#         "sudo gitlab-runner unregister --all-runners",
#       ]
#   }

# }